# static bugzilla miscweb static websites

Blubber images for old bugzilla static html sites.

Mirror of https://gerrit.wikimedia.org/r/plugins/gitiles/operations/container/miscweb/

HTML content is compressed (gz) in Git to decrease disk usage and git overhead. During the blubber image build everything gets uncompressed and copied to the container image. Apache serves uncompressed content due to performance issues (see [T343914](https://phabricator.wikimedia.org/T343914)).

## Local development

Build image locally:
```
DOCKER_BUILDKIT=1 docker build --target production -f .pipeline/blubber.yaml .
```

Run image:
```
docker run -p 8080:8080 <image name>
```

Visit `http://127.0.0.1:8080/`

### Manipulate and extract content

All html is stored in `html-compressed/static-bugzilla.gz` and unzipped during image build. This safes space and increases performance in git. The archive is also stored in lfs.

To modify html files, first unzip the archive:

```
tar -xvf ./html-compressed/static-bugzilla.gz -C ./html-compressed/
```

Make your changes to the html files in `/html-compressed/static-bugzilla/`.

Then compress the html files again:

```
cd html-compressed; tar -czvf static-bugzilla.gz static-bugzilla/; rm -R ./static-bugzilla
```

## Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/bugzilla:<timestamp>`
